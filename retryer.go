package retryer

import (
	"log"
	"time"
)

type retryer struct {
	retries    uint64
	errorPause time.Duration
	logErrors  bool
}

func New(retries uint64, errorPause time.Duration, logErrors bool) *retryer {
	return &retryer{retries, errorPause, logErrors}
}

func (p *retryer) Do(f func() error) (err error) {

	for retry := uint64(0); retry < p.retries; retry++ {
		err = f()
		if err != nil {
			if p.logErrors {
				log.Printf("retry error (%d): %s", retry, err.Error())
			}
			time.Sleep(p.errorPause)
			continue
		}
		break
	}

	return
}
